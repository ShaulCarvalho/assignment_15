PRAGMA table_info(accounts);
begin transaction;
update accounts set balance=800 where id=3;
update accounts set balance=00 where id=1;
update accounts set balance=950 where id=2;
update accounts set balance=-250 where id=1;
commit;